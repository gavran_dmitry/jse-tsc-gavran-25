package ru.tsc.gavran.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @Nullable
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list commands.";
    }

    @Override
    public void execute() {
        int index = 1;
        final HashMap<String, String> cmd = new HashMap<>();
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            cmd.put(command.name(), command
                    .getClass()
                    .getPackage()
                    .getName()
                    .substring(command.getClass().getPackage().getName().lastIndexOf(".") + 1).toUpperCase());
        }
        final Map<String, List<String>> valueMap = cmd.keySet().stream().collect(Collectors.groupingBy(k -> cmd.get(k)));
        for (String s : valueMap.keySet()) {
            System.out.println("\n" + s + " COMMAND:");
            for (String str : valueMap.get(s)) {
                System.out.println(index + ". " + str);
                index++;
            }
        }
    }
}