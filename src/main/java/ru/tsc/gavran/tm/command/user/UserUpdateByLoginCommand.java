package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.entity.UserNotFoundException;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.model.User;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserUpdateByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-update-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user info by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (!currentUserId.equals(user.getId())) throw new AccessDeniedException();
        System.out.println("ENTER FIRST NAME: ");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUserByLogin(login, lastName, firstName, middleName, email);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }
}