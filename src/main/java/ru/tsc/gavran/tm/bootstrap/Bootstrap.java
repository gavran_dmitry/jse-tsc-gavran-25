package ru.tsc.gavran.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.gavran.tm.api.repository.*;
import ru.tsc.gavran.tm.api.service.*;
import ru.tsc.gavran.tm.command.AbstractCommand;
import ru.tsc.gavran.tm.command.system.ExitCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.exception.system.UnknownCommandException;
import ru.tsc.gavran.tm.repository.*;
import ru.tsc.gavran.tm.service.*;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @NotNull
    private final IAuthService authService = new AuthService(authRepository, userService, propertyService);

    @NotNull
    private final LogScanner logScanner = new LogScanner();

    public void start(String[] args) {
        logScanner.init();
        displayWelcome();
        initUsers();
        initCommands();
        initData();
        parseArgs(args);
        process();
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.gavran.tm.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.gavran.tm.command.AbstractCommand.class)
                .stream()
                .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            try {
                registry(clazz.newInstance());
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    public void initUsers() {
        userService.create("Test", "Test", Role.USER);
        userService.updateUserByLogin("Test", "LTest1", "FTest", "MTest", "test@gmail.com");
        userService.create("Admin", "Admin", Role.ADMIN);
        userService.updateUserByLogin("Admin", "GAVRAN", "Dmitry", "Mihailovich", "gavran@gmail.com");
    }

    private void initData() {
        projectService.create(userRepository.findByLogin("Test").getId(), "Project C", "1");
        projectService.create(userRepository.findByLogin("Test").getId(), "Project A", "2");
        projectService.create(userRepository.findByLogin("Admin").getId(), "Project B", "3");
        projectService.create(userRepository.findByLogin("Admin").getId(), "Project D", "4");
        taskService.create(userRepository.findByLogin("Test").getId(), "Task C", "1");
        taskService.create(userRepository.findByLogin("Test").getId(), "Task A", "2");
        taskService.create(userRepository.findByLogin("Admin").getId(), "Task B", "3");
        taskService.create(userRepository.findByLogin("Admin").getId(), "Task D", "4");
        projectService.finishByName(userRepository.findByLogin("Test").getId(), "Project C");
        projectService.startByName(userRepository.findByLogin("Admin").getId(), "Project D");
        taskService.finishByName(userRepository.findByLogin("Test").getId(), "Task C");
        taskService.startByName(userRepository.findByLogin("Admin").getId(), "Task B");
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void displayAuth() {
        System.out.println("You are not authorized. " + "Enter one of the following commands: " + "\n" + "\n"
                + commandService.getCommandByName("login") + "\n"
                + commandService.getCommandByName("exit") + "\n"
                + commandService.getCommandByName("user-create") + "\n"
                + commandService.getCommandByName("about") + "\n"
                + commandService.getCommandByName("version") + "\n");
    }

    private void process() {
        logService.debug("Logging started.");
        String exit = new ExitCommand().name();
        @NotNull
        String command = "";
        while (!exit.equals(command)) {
            try {
                while (!getAuthService().isAuth()) { //работа с неаутентифицированными пользователями
                    try {
                        displayAuth();
                        command = TerminalUtil.nextLine();
                        switch (command) {
                            case "exit":
                                parseCommand("exit");
                                break;
                            case "login":
                                parseCommand("login");
                                break;
                            case "user-create":
                                parseCommand("user-create");
                                break;
                            case "about":
                                parseCommand("about");
                                break;
                            case "version":
                                parseCommand("version");
                                break;
                            case "" :
                                break;
                            default:
                                throw new AccessDeniedException();
                        }
                    } catch (@NotNull final Exception e) {
                        logService.error(e);
                    }
                }
                System.out.println("ENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.debug(command);
                parseCommand(command);
                logService.debug("Completed.");
            } catch (@NotNull final Exception e) {
                logService.error(e);
            }
        }
    }

    public void parseArg(@Nullable final String arg) {
        @Nullable
        AbstractCommand arguments = commandService.getCommandByArg(arg);
        if (!Optional.ofNullable(arguments).isPresent()) throw new UnknownCommandException();
        arguments.execute();
    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent() || command.isEmpty()) return;
        @Nullable
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (!Optional.ofNullable(abstractCommand).isPresent()) throw new UnknownCommandException();
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    private void registry(@Nullable AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

}