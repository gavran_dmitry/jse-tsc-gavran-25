package ru.tsc.gavran.tm.api.entity;

public interface IWBS extends IHasStartDate, IHasName, IHasCreated, IHasStatus {
}
