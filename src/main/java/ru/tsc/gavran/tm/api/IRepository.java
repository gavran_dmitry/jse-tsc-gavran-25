package ru.tsc.gavran.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gavran.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity);

    void remove(final E entity);

    @NotNull
    List<E> findAll();

    void clear();

    @NotNull
    E findById(@NotNull final String id);

    @NotNull
    E removeById(@NotNull final String id);

    @NotNull
    E findByIndex(@NotNull final Integer index);

    @NotNull
    List<E> findAll(@NotNull final Comparator<E> comparator);

    boolean existsById(@NotNull final String id);

    @NotNull
    E removeByIndex(@NotNull final Integer index);

}