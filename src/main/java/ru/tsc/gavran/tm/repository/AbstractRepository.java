package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(@NotNull final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        list.sort(comparator);
        return list;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @NotNull
    @Override
    public E findById(@NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(ProcessException::new);
    }

    @NotNull
    @Override
    public E removeById(@NotNull final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(ProcessException::new);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final Integer index) {
        if (list == null) return null;
        return list.get(index);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }
}
